<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "urgency".
 *
 * @property integer $urgencyNumber
 * @property string $urgencyName
 *
 * @property Project[] $projects
 */
class Urgency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'urgency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['urgencyName'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'urgencyNumber' => 'Urgency Number',
            'urgencyName' => 'Urgency Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Project::className(), ['urgencyId' => 'urgencyNumber']);
    }
}
