<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "project".
 *
 * @property integer $projectId
 * @property string $projectName
 * @property string $startDate
 * @property string $endDate
 * @property string $planeDate
 * @property string $location
 * @property string $description
 * @property integer $userId
 * @property integer $taskId
 * @property integer $urgencyId
 * @property integer $statusId
 *
 * @property Status $status
 * @property Task $task
 * @property Urgency $urgency
 * @property User $user
 */
class Project extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['startDate', 'endDate', 'planeDate'], 'safe'],
            [['userId', 'taskId', 'urgencyId', 'statusId'], 'integer'],
            [['projectName', 'location', 'description'], 'string', 'max' => 255],
            [['statusId'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['statusId' => 'statusNumber']],
            [['taskId'], 'exist', 'skipOnError' => true, 'targetClass' => Task::className(), 'targetAttribute' => ['taskId' => 'taskId']],
            [['urgencyId'], 'exist', 'skipOnError' => true, 'targetClass' => Urgency::className(), 'targetAttribute' => ['urgencyId' => 'urgencyNumber']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'projectId' => 'Project ID',
            'projectName' => 'Project Name',
            'startDate' => 'Start Date',
            'endDate' => 'End Date',
            'planeDate' => 'Plane Date',
            'location' => 'Location',
            'description' => 'Description',
            'userId' => 'User ID',
            'taskId' => 'Task ID',
            'urgencyId' => 'Urgency ID',
            'statusId' => 'Status ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['statusNumber' => 'statusId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['taskId' => 'taskId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUrgency()
    {
        return $this->hasOne(Urgency::className(), ['urgencyNumber' => 'urgencyId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }
}
