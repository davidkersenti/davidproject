<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property string $firstName
 * @property string $lastName
 * @property string $email
 * @property string $address
 * @property integer $id
 * @property integer $roleId
 * @property string $username
 * @property string $password
 * @property string $authKey
 * @property string $created_at
 *
 * @property Project[] $projects
 * @property Task[] $tasks
 * @property Role $role
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['roleId'], 'integer'],
            [['firstName', 'lastName', 'email', 'address', 'username', 'password', 'authKey', 'created_at'], 'string', 'max' => 255],
            [['roleId'], 'exist', 'skipOnError' => true, 'targetClass' => Role::className(), 'targetAttribute' => ['roleId' => 'roleNumber']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'firstName' => 'First Name',
            'lastName' => 'Last Name',
            'email' => 'Email',
            'address' => 'Address',
            'id' => 'ID',
            'roleId' => 'Role ID',
            'username' => 'Username',
            'password' => 'Password',
            'authKey' => 'Auth Key',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Project::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Task::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole()
    {
        return $this->hasOne(Role::className(), ['roleNumber' => 'roleId']);
    }
}
