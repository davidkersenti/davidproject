<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "task".
 *
 * @property integer $taskId
 * @property string $taskName
 * @property string $startDate
 * @property string $endDate
 * @property string $planeDate
 * @property string $description
 * @property integer $levelId
 * @property integer $userId
 * @property integer $statusId
 * @property string $created_at
 *
 * @property Project[] $projects
 * @property Level $level
 * @property Status $status
 * @property User $user
 */
class Task extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['startDate', 'endDate', 'planeDate'], 'safe'],
            [['levelId', 'userId', 'statusId'], 'integer'],
            [['taskName', 'description', 'created_at'], 'string', 'max' => 255],
            [['levelId'], 'exist', 'skipOnError' => true, 'targetClass' => Level::className(), 'targetAttribute' => ['levelId' => 'levelNumber']],
            [['statusId'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['statusId' => 'statusNumber']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'taskId' => 'Task ID',
            'taskName' => 'Task Name',
            'startDate' => 'Start Date',
            'endDate' => 'End Date',
            'planeDate' => 'Plane Date',
            'description' => 'Description',
            'levelId' => 'Level ID',
            'userId' => 'User ID',
            'statusId' => 'Status ID',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Project::className(), ['taskId' => 'taskId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLevel()
    {
        return $this->hasOne(Level::className(), ['levelNumber' => 'levelId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['statusNumber' => 'statusId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }
}
