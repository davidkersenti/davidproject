<?php

use yii\db\Migration;

class m170808_133554_init_project_table extends Migration
{
    
     public function up()
    {

     $this->createTable('project', [
             'projectId'  => 'pk',
			'projectName' => 'string',
			'startDate' => 'date',
			'endDate' => 'date',
            'planeDate'  => 'date' ,
            'location' => 'string',
             'description'  => 'string',
            
        'userId'  => 'integer', // פתח זר
        'taskId'  => 'integer', // פתח זר
           
            'urgencyId'  => 'integer', // פתח זר
            'statusId' => 'integer', // פתח זר
		]);
         $this->addForeignKey(
            'fk-project-userId',// This is the fk => the table where i want the fk will be
            'project',// son table
            'userId', // son pk	
            'user', // father table
            'id', // father pk
            'CASCADE'
			);

            $this->addForeignKey(
            'fk-project-taskId',// This is the fk => the table where i want the fk will be
            'project',// son table
            'taskId', // son pk	
            'task', // father table
            'taskId', // father pk
            'CASCADE'
			);

            $this->addForeignKey(
            'fk-project-urgencyId',// This is the fk => the table where i want the fk will be
            'project',// son table
            'urgencyId', // son pk	
            'urgency', // father table
            'urgencyNumber', // father pk
            'CASCADE'
			);

            $this->addForeignKey(
            'fk-project-statusId',// This is the fk => the table where i want the fk will be
            'project',// son table
            'statusId', // son pk	
            'status', // father table
            'statusNumber', // father pk
            'CASCADE'
			);
            



    }
             

    public function down()
    {
        echo "m170808_133554_init_project_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
