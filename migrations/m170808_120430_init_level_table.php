<?php

use yii\db\Migration;

class m170808_120430_init_level_table extends Migration
{
    public function up()
    {

        $this->createTable('level', [
            'levelNumber' => 'pk' ,
            'levelname'  => 'string',
            
		]);
   }

    public function down()
    {
        echo "m170808_120430_init_level_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
