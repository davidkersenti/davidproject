<?php

use yii\db\Migration;

class m170808_114324_init_role_table extends Migration
{
   public function up()
    {

        $this->createTable('role', [
            'roleNumber' => 'pk' ,
            'rolename'  => 'string',
            
		]);
   }
    public function down()
    {
        echo "m170808_114324_init_role_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
