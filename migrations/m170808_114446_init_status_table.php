<?php

use yii\db\Migration;

class m170808_114446_init_status_table extends Migration
{
    public function up()
    {

        $this->createTable('status', [
            'statusNumber' => 'pk' ,
            'statusname'  => 'string',
            
		]);
   }

    public function down()
    {
        echo "m170808_114446_init_status_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
