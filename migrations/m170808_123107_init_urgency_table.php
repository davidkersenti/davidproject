<?php

use yii\db\Migration;

class m170808_123107_init_urgency_table extends Migration
{
    public function up()
    {

        $this->createTable('urgency', [
            'urgencyNumber' => 'pk' ,
            'urgencyName'  => 'string',
            
		]);
   
    }
    public function down()
    {
        echo "m170808_123107_init_urgency_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
