<?php

use yii\db\Migration;

class m170808_121823_init_task_table extends Migration
{
       public function up()
    {

             $this->createTable('task', [
             'taskId'  => 'pk',
			'taskName' => 'string',
			'startDate' => 'date',
			'endDate' => 'date',
            'planeDate'  => 'date' ,
            'description'  => 'string',
            'levelId'  => 'integer', // פתח זר
            'userId' => 'integer', /// מפתח זר
            'statusId' => 'integer', ///מפתח זר
			'created_at'  => 'string',
		]);
        
        	 $this->addForeignKey(
            'fk-task-levelId',// This is the fk => the table where i want the fk will be
            'task',// son table
            'levelId', // son pk	
            'level', // father table
            'levelNumber', // father pk
            'CASCADE'
			);

             $this->addForeignKey(
            'fk-task-userId',// This is the fk => the table where i want the fk will be
            'task',// son table
            'userId', // son pk	
            'user', // father table
            'id', // father pk
            'CASCADE'
			);

               $this->addForeignKey(
            'fk-task-statusId',// This is the fk => the table where i want the fk will be
            'task',// son table
            'statusId', // son pk	
            'status', // father table
            'statusNumber', // father pk
            'CASCADE'
			);



    }

    public function down()
    {
        echo "m170808_121823_init_task_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
