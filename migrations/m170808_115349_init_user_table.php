<?php

use yii\db\Migration;

class m170808_115349_init_user_table extends Migration
{
    public function up()
    {

             $this->createTable('user', [
                 'firstName'  => 'string',
			'lastName' => 'string',
			'email' => 'string',
			'address' => 'string',
            'id'  => 'pk' ,
            'roleId'  => 'integer',
            'username'  => 'string',
            'password' => 'string',
            'authKey'  => 'string',
			
			'created_at'  => 'string',
		]);


        				 $this->addForeignKey(
            'fk-user-roleId',// This is the fk => the table where i want the fk will be
            'user',// son table
            'roleId', // son pk	
            'role', // father table
            'roleNumber', // father pk
            'CASCADE'
			);

    }

    public function down()
    {
        echo "m170808_115349_init_user_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
